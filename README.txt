The Contentout module creates CSV files from Drupal nodes. It uses a
hierarchical structure in the CSV file to preserve node hierarchy. For defaults,
Field/subfield columns are separated by three underscores and muti-value fields
are separated by two underscores. The default delimiter is a comma.

Date fields are exported in DateTime format (i.e. not Unixtime) for easier
migration into Drupal.

The Contentout module is a companion module to the Contentin module, as the
former creates CSV files that are more easily imported by the latter. Both
modules appear in the Content Automation portion of the modules page.

This module depends on the Migrate module (http://drupal.org/project/migrate)
to dynamically register migrations from CSV files.

Compatibility
-------------
The Contentin requires Migrate V2.6 or later.


Maintainer
-----------
Hector Iribarne https://www.drupal.org/user/192646
